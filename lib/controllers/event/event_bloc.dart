import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:suitmedia_screening_test_agung/models/event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'event_event.dart';

part 'event_state.dart';

class EventBloc extends Bloc<EventEvent, EventState> {
  EventBloc() : super(EventState());
  var isAnimating = false;
  var isScrolling = false;

  @override
  Stream<EventState> mapEventToState(
    EventEvent event,
  ) async* {
    final selectedIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
        devicePixelRatio: 4,
      ),
      'assets/images/ic_marker_selected.png',
    );
    final unSelectedIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(
        devicePixelRatio: 4,
      ),
      'assets/images/ic_marker_unselected.png',
    );

    if (event is OnLoadEvent) {
      List<Marker> markers = _generateMarker(
        listEvent,
        -1,
        unSelectedIcon,
        selectedIcon,
      );
      yield state.copyWith(events: listEvent, markers: markers);
    } else if (event is OnSelectEvent) {
      List<Marker> markers = _generateMarker(
        state.events,
        event.selectedId,
        unSelectedIcon,
        selectedIcon,
      );
      yield state.copyWith(markers: markers, selectedId: event.selectedId);
    } else if (event is OnResetMarker) {
      List<Marker> markers = _generateMarker(
        state.events,
        -1,
        unSelectedIcon,
        selectedIcon,
      );
      yield state.copyWith(markers: markers);
    }
  }

  List<Marker> _generateMarker(
    List<Event> events,
    int selectedEventId,
    BitmapDescriptor unSelectedIcon,
    BitmapDescriptor selectedIcon,
  ) {
    List<Marker> markers = [];
    events.forEach((e) {
      markers.add(Marker(
          markerId: MarkerId(
            e.name,
          ),
          icon: selectedEventId == e.id ? selectedIcon : unSelectedIcon,
          position: LatLng(e.latitude, e.longitude),
          onTap: () {
            this.add(OnSelectEvent(e.id));
          }));
    });

    return markers;
  }
}
