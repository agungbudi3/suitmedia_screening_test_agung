part of 'event_bloc.dart';

@immutable
abstract class EventEvent {}

class OnLoadEvent extends EventEvent{}
class OnSelectEvent extends EventEvent{
  final int selectedId;

  OnSelectEvent(this.selectedId);

}
class OnResetMarker extends EventEvent{}
