part of 'event_bloc.dart';

class EventState {
  final List<Event> events;
  final List<Marker> markers;
  final int selectedId;
  static const List<Event> defaultEvent = [];
  static const List<Marker> defaultMarker = [];

  EventState(
      {this.events = defaultEvent,
      this.markers = defaultMarker,
      this.selectedId = -1});

  EventState copyWith({
    List<Event>? events,
    List<Marker>? markers,
    int? selectedId,
  }) {
    return EventState(
      events: events ?? this.events,
      markers: markers ?? this.markers,
      selectedId: selectedId ?? -1,
    );
  }
}
