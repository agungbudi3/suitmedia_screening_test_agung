import 'package:bloc/bloc.dart';

enum EventView { LIST, MAP }

class EventViewCubit extends Cubit<EventView> {
  EventViewCubit() : super(EventView.LIST);

  toggleView() {
    switch (state) {
      case EventView.LIST:
        emit(EventView.MAP);
        break;
      case EventView.MAP:
        emit(EventView.LIST);
        break;
    }
  }
}
