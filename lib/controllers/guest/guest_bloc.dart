import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';
import 'package:suitmedia_screening_test_agung/models/guest.dart';
import 'package:suitmedia_screening_test_agung/utils/endpoints.dart';
import 'package:suitmedia_screening_test_agung/utils/network/network_exceptions.dart';

part 'guest_event.dart';

part 'guest_state.dart';

class GuestBloc extends Bloc<GuestEvent, GuestState> with HydratedMixin {
  GuestBloc(this.endpoints) : super(GuestState());
  final Endpoints endpoints;

  @override
  Stream<GuestState> mapEventToState(
    GuestEvent event,
  ) async* {
    if (event is OnRefreshGuest) {
      yield (state.copyWith(isRefreshing: true));

      var res = await endpoints.getGuest();
      yield* res.when(
        success: (guestRes) async* {
          List<Guest> guest = [];
          guest.addAll(guestRes.data);
          var currentPage = guestRes.page;
          var hasNext = guestRes.totalPages > currentPage;

          yield state.copyWith(
            guests: guest,
            currentPage: currentPage,
            hasNext: hasNext,
          );
        },
        failure: (NetworkExceptions error) async* {
          yield state.copyWith(
            isError: true,
            errorMessage: NetworkExceptions.getErrorMessage(error),
          );
        },
      );
    } else if (event is OnLoadGuest) {
      int page = state.currentPage + 1;
      yield (state.copyWith(isLoading: true));

      var res = await endpoints.getGuest(page: page);
      yield* res.when(
        success: (guestRes) async* {
          List<Guest> guest = state.guests;
          guest.addAll(guestRes.data);
          var currentPage = guestRes.page;
          var hasNext = guestRes.totalPages > currentPage;

          yield state.copyWith(
            guests: guest,
            currentPage: currentPage,
            hasNext: hasNext,
          );
        },
        failure: (NetworkExceptions error) async* {
          yield state.copyWith(
            isError: true,
            errorMessage: NetworkExceptions.getErrorMessage(error),
          );
        },
      );
    }
  }

  @override
  GuestState? fromJson(Map<String, dynamic> json) {
    return GuestState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(GuestState state) {
    return state.toMap();
  }
}
