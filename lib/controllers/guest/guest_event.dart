part of 'guest_bloc.dart';

@immutable
abstract class GuestEvent {}

class OnRefreshGuest extends GuestEvent {}

class OnLoadGuest extends GuestEvent {}
