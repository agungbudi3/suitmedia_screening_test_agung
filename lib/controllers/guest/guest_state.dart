part of 'guest_bloc.dart';

class GuestState {
  static const List<Guest> defaultValue = [];
  final List<Guest> guests;
  final int currentPage;
  final bool hasNext;
  final bool isRefresh;
  final bool isLoading;
  final bool isError;
  final String errorMessage;

  GuestState({
    this.guests = defaultValue,
    this.currentPage = 1,
    this.hasNext = false,
    this.isRefresh = false,
    this.isLoading = false,
    this.isError = false,
    this.errorMessage = '',
  });

  GuestState copyWith({
    List<Guest>? guests,
    int? currentPage,
    bool? hasNext,
    bool? isRefreshing,
    bool? isLoading,
    bool? isError,
    String? errorMessage,
  }) {
    return GuestState(
      guests: guests ?? this.guests,
      currentPage: currentPage ?? this.currentPage,
      hasNext: hasNext ?? this.hasNext,
      isRefresh: isRefreshing ?? false,
      isLoading: isLoading ?? false,
      isError: isError ?? false,
      errorMessage: errorMessage ?? "",
    );
  }

  factory GuestState.fromMap(Map<String, dynamic> map) {
    List<Guest> guests = [];
    if(map['guests'] != null){
      map['guests'].forEach((v) {
        guests.add(Guest.fromJson(v));
      });
    }
    return new GuestState(
      guests: guests,
      currentPage: map['currentPage'] as int,
      hasNext: map['hasNext'] as bool,
      isRefresh: map['isRefresh'] as bool,
      isLoading: map['isLoading'] as bool,
      isError: map['isError'] as bool,
      errorMessage: map['errorMessage'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'guests': this.guests.map((v) => v.toJson()).toList(),
      'currentPage': this.currentPage,
      'hasNext': this.hasNext,
      'isRefresh': this.isRefresh,
      'isLoading': this.isLoading,
      'isError': this.isError,
      'errorMessage': this.errorMessage,
    } as Map<String, dynamic>;
  }

  String toJson() => json.encode(toMap());

  factory GuestState.fromJson(String source) =>
      GuestState.fromMap(jsonDecode(source));
}
