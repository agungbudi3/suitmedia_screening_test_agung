import 'package:bloc/bloc.dart';

class InputNameCubit extends Cubit<String> {
  InputNameCubit() : super("");

  setName(String input) => emit(input);
}
