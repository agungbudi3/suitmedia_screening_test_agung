import 'package:bloc/bloc.dart';

class InputPalindromeCubit extends Cubit<String> {
  InputPalindromeCubit() : super("");

  setPalindrome(String input) => emit(input);
}
