import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:suitmedia_screening_test_agung/models/event.dart';
import 'package:suitmedia_screening_test_agung/models/guest.dart';

part 'main_event.dart';

part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  MainBloc() : super(MainState());

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if (event is OnUpdateName) {
      yield state.copyWith(name: event.name);
    } else if (event is OnUpdateEvent) {
      yield state.copyWith(event: event.event);
    } else if (event is OnUpdateGuest) {
      yield state.copyWith(guest: event.guest);
    }
  }
}
