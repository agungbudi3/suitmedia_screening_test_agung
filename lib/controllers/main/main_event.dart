part of 'main_bloc.dart';

@immutable
abstract class MainEvent {}

class OnUpdateName extends MainEvent {
  final String name;

  OnUpdateName(this.name);
}

class OnUpdateEvent extends MainEvent {
  final Event event;

  OnUpdateEvent(this.event);
}

class OnUpdateGuest extends MainEvent {
  final Guest guest;

  OnUpdateGuest(this.guest);
}
