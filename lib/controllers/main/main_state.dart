part of 'main_bloc.dart';

class MainState {
  final String name;
  final Event? event;
  final Guest? guest;

  MainState({
    this.name = '',
    this.event,
    this.guest,
  });

  MainState copyWith({
    String? name,
    Event? event,
    Guest? guest,
  }) {
    return MainState(
      name: name ?? this.name,
      event: event ?? this.event,
      guest: guest ?? this.guest,
    );
  }
}
