import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'palindrome_state.dart';

class PalindromeCubit extends Cubit<PalindromeState> {
  PalindromeCubit() : super(PalindromeInitial());

  checkPalindrome(String input) {
    if (input.isNotEmpty) {
      if (_isPalindrome(input)) {
        emit(IsPalindrome());
      } else {
        emit(IsNotPalindrome());
      }
    }
  }

  _isPalindrome(String input) {
    String word = input.toUpperCase().replaceAll(new RegExp(r"\s+"), "");

    for (int i = 0; i < word.length ~/ 2; i++) {
      if (word[i] != word[word.length - i - 1]) return false;
    }
    return true;
  }
}
