part of 'palindrome_cubit.dart';

@immutable
abstract class PalindromeState {}

class PalindromeInitial extends PalindromeState {}

class IsPalindrome extends PalindromeState {}

class IsNotPalindrome extends PalindromeState {}
