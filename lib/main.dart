import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';
import 'package:suitmedia_screening_test_agung/utils/endpoints.dart';
import 'package:suitmedia_screening_test_agung/utils/providers.dart';
import 'package:suitmedia_screening_test_agung/utils/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return RepositoryProvider(
        create: (c) => Endpoints(),
        child: MultiBlocProvider(
          providers: bloc,
          child: MaterialApp(
            title: 'Screening Test Agung',
            theme: ThemeData(
              primarySwatch: Colors.orange,
              primaryColor: kPrimaryColor,
              scaffoldBackgroundColor: Colors.white,
            ),
            initialRoute: firstRoute,
            routes: routes,
          ),
        ));
  }
}
