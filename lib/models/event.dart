class Event {
  final int id;
  final String name, description, date, image;
  final double latitude, longitude;

  Event({
    required this.id,
    required this.name,
    required this.description,
    required this.date,
    required this.image,
    required this.latitude,
    required this.longitude,
  });
}

List<Event> listEvent = [
  Event(
    id: 1,
    name: "RADIOVERSE 2021: “What’s Inside Radio’s Life?”",
    description: "RADIOVERSE 2021 ini sendiri merupakan sebuah ajang untuk mengenalkan serta mendidik kaum muda seputar dunia Radio yang nantinya berisikan oleh Talk Show Session dan Mini Workshop yang akan dibawakan oleh para pembicara yang ahli pada bidangnya.",
    date: "June 5, 2021",
    image:
        "https://eventbandung.id/wp-content/uploads/2021/06/MP-EB-RADIOVERSE-2021-Unpar-Radio-Station-URS-Copy.jpg",
    latitude: -6.945097863373454,
    longitude: 107.66037368896856,
  ),
  Event(
    id: 2,
    name: "MGTC IX",
    description: "We proudly present national webinar x talkshow which is the largest event held by IMAsc Telkom University with theme “Build a Meaningful Business to Survive The Pandemic Era”. With our mc Gayatri Puspita and Yesica Naomi.",
    date: "June 5, 2021",
    image:
        "https://eventbandung.id/wp-content/uploads/2021/05/MP-EB-MGTC-IX-IMAsc-Telkom-University-Copy.jpg",
    latitude: -6.938281741714445,
    longitude: 107.63067627071231,
  ),
  Event(
    id: 3,
    name: "Certified Associate in Supply Chain BATCH 2",
    description: "Ingin belajar dari dasar terkait konsep dan pengertian rantai pasok? CASC merupakan pilihan yang tepat Temukan hal-hal penting dari manajemen rantai pasok dan bagaimana memberikan solusi efektif untuk risk exposures.",
    date: "June 27, 2021",
    image:
        "https://eventbandung.id/wp-content/uploads/2021/05/MP-EB-Certified-Associate-in-Supply-Chain-BATCH-02-ISCQAcademy-1921232527-Juni-2021-Copy.jpg",
    latitude: -6.954640267410873,
    longitude: 107.63170623890905,
  ),
  Event(
    id: 4,
    name: "Seminar Profesi Kesehatan Masyarakat Series 1",
    description: "Tetap Sehat Mental Selama Pandemi",
    date: "June 12, 2021",
    image:
        "https://eventbandung.id/wp-content/uploads/2021/05/MP-EB-Seminar-Profesi-Kesehatan-Masyarakat-Series-1-Mahasiswa-Kesehatan-Masyarakat-Universitas-Esa-Unggul-Copy.jpg",
    latitude: -6.953447477485305,
    longitude: 107.67033004628601,
  ),
];
