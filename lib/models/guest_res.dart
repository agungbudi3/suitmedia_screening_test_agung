import 'package:suitmedia_screening_test_agung/models/guest.dart';

class GuestRes {
  static const List<Guest> defaultData = [];
  int _page = 0;
  int _perPage = 0;
  int _total = 0;
  int _totalPages = 0;
  List<Guest> _data = defaultData;

  int get page => _page;

  int get perPage => _perPage;

  int get total => _total;

  int get totalPages => _totalPages;

  List<Guest> get data => _data;

  GuestRes({
    int page = 0,
    int perPage = 0,
    int total = 0,
    int totalPages = 0,
    List<Guest> data = defaultData,
  }) {
    _page = page;
    _perPage = perPage;
    _total = total;
    _totalPages = totalPages;
    _data = data;
  }

  GuestRes.fromJson(dynamic json) {
    _page = json["page"];
    _perPage = json["per_page"];
    _total = json["total"];
    _totalPages = json["total_pages"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Guest.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["page"] = _page;
    map["per_page"] = _perPage;
    map["total"] = _total;
    map["total_pages"] = _totalPages;
    map["data"] = _data.map((v) => v.toJson()).toList();
    return map;
  }
}
