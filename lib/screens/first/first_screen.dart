import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:suitmedia_screening_test_agung/controllers/palindrome/palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/card_input.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/container_avatar.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/footer_copyright.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/header_welcome.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_name.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_next.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    var headerHeight = 221 / 360 * screenWidth;
    return Scaffold(
      body: BlocListener<PalindromeCubit, PalindromeState>(
        listener: (c, s) {
          if (s is IsPalindrome) {
            Fluttertoast.showToast(
              msg: "Is Palindrome",
            );
          } else if (s is IsNotPalindrome) {
            Fluttertoast.showToast(
              msg: "Not Palindrome",
            );
          }
        },
        child: SingleChildScrollView(
          child: Container(
            height: screenHeight,
            child: Stack(
              children: [
                HeaderWelcome(),
                Positioned(
                  top: headerHeight - 56,
                  left: kDefaultPadding * 1.5,
                  right: kDefaultPadding * 1.5,
                  child: CardInput(),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: FooterCopyright(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
