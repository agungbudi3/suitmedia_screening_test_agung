import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/container_avatar.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_check.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_name.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_next.dart';
import 'package:suitmedia_screening_test_agung/screens/first/widgets/input_palindrome.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class CardInput extends StatelessWidget {
  const CardInput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: kDefaultPadding,
          vertical: kDefaultPadding * 1.5,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            ContainerAvatar(),
            SizedBox(height: kDefaultPadding * 1.5),
            InputName(),
            SizedBox(height: kDefaultPadding / 1.5),
            InputPalindrome(),
            SizedBox(height: kDefaultPadding * 1.5),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: InputNext(),
                ),
                SizedBox(width: kDefaultPadding / 2),
                Expanded(
                  flex: 1,
                  child: InputCheck(),
                ),
              ],
            ),
            SizedBox(height: kDefaultPadding / 2),
          ],
        ),
      ),
    );
  }
}
