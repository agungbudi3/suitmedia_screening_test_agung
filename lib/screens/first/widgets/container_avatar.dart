import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class ContainerAvatar extends StatelessWidget {
  const ContainerAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: kPrimaryColor,
        shape: BoxShape.circle,
      ),
      child: Image.asset("assets/images/img_avatar.png"),
    );
  }
}
