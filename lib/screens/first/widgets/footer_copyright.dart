import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class FooterCopyright extends StatelessWidget {
  const FooterCopyright({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(kDefaultPadding * 1.5),
      child: Text(
        "Copyright © 2020 Suitmedia All right reserved",
        textAlign: TextAlign.center,
      ),
    );
  }
}
