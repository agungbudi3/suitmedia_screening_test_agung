import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class HeaderWelcome extends StatelessWidget {
  const HeaderWelcome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 360 / 221,
      child: Container(
        color: Color(0xffd7844c),
        child: Stack(
          children: [
            Image.asset(
              "assets/images/bg_bright.png",
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: kDefaultPadding / 2,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Welcome",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    ),
                  ),
                  SizedBox(height: kDefaultPadding / 2),
                  Text(
                    "This is app for suitmedia mobile developer test",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}