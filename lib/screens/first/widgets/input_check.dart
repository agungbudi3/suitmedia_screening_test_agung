import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_palindrome/input_palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_name/input_name_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/palindrome/palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_elevated_button.dart';
import 'package:suitmedia_screening_test_agung/utils/routes.dart';

class InputCheck extends StatelessWidget {
  const InputCheck({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String text = context.watch<InputPalindromeCubit>().state;
    Function()? press = text.isNotEmpty ? () {
      context.read<PalindromeCubit>().checkPalindrome(text);
    } : null;

    return MyElevatedButton(press: press, text: "Check");
  }
}
