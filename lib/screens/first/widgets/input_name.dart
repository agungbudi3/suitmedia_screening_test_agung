import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_name/input_name_cubit.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_text_field.dart';

class InputName extends StatefulWidget {
  const InputName({Key? key}) : super(key: key);

  @override
  _InputNameState createState() => _InputNameState();
}

class _InputNameState extends State<InputName> {
  TextEditingController _nameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _nameController.text = context.read<InputNameCubit>().state;
  }

  @override
  Widget build(BuildContext context) {
    return MyTextField(
      controller: _nameController,
      textCapitalization: TextCapitalization.words,
      onChanged: (value) {
        context.read<InputNameCubit>().setName(value);
      },
      hintText: "Type name here..",
    );
  }
}
