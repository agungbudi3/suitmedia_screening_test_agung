import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_name/input_name_cubit.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_elevated_button.dart';
import 'package:suitmedia_screening_test_agung/utils/routes.dart';

class InputNext extends StatelessWidget {
  const InputNext({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = context.watch<InputNameCubit>().state;
    Function()? press = name.isNotEmpty
        ? () {
            context.read<MainBloc>().add(OnUpdateName(name));
            Navigator.pushNamed(context, secondRoute);
          }
        : null;

    return MyElevatedButton(press: press, text: "Next");
  }
}
