import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_name/input_name_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_palindrome/input_palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_text_field.dart';

class InputPalindrome extends StatefulWidget {
  const InputPalindrome({Key? key}) : super(key: key);

  @override
  _InputPalindromeState createState() => _InputPalindromeState();
}

class _InputPalindromeState extends State<InputPalindrome> {
  TextEditingController _palindromeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _palindromeController.text = context.read<InputPalindromeCubit>().state;
  }

  @override
  Widget build(BuildContext context) {
    return MyTextField(
      controller: _palindromeController,
      onChanged: (value) {
        context.read<InputPalindromeCubit>().setPalindrome(value);
      },
      hintText: "Type text palindrome",
    );
  }
}
