import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/screens/fourth/widgets/guest_grid.dart';

class FourthScreen extends StatelessWidget {
  const FourthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "GUESTS",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 19,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: false,
      ),
      body: GuestGrid(
        onItemClick: (guest) {
          Fluttertoast.showToast(
            msg: _getMessage(guest.id),
          );
          context.read<MainBloc>().add(OnUpdateGuest(guest));
          Navigator.pop(context);
        },
      ),
    );
  }

  String _getMessage(int guestId) {
    bool isMod2 = guestId % 2 == 0;
    bool isMod3 = guestId % 3 == 0;
    var message = 'Feature Phone';
    if (isMod2 && isMod3) {
      message = 'IOS';
    } else if (isMod2) {
      message = 'Blackberry';
    } else if (isMod3) {
      message = 'Android';
    }

    message += _isPrime(guestId) ? " PRIME" : " NON PRIME";

    return message;
  }

  bool _isPrime(int guestId) {
    if (guestId <= 3) return guestId > 1;
    if ((guestId % 2 == 0) || (guestId % 3 == 0)) return false;
    var count = 5;
    while (count * count <= guestId) {
      if (guestId % count == 0 || guestId % (count + 2) == 0) return false;
    }

    return true;
  }
}
