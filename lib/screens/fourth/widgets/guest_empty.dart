import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/controllers/guest/guest_bloc.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GuestEmpty extends StatelessWidget {
  const GuestEmpty({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(kDefaultPadding * 2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.warning_amber_outlined,
            color: Colors.orange,
            size: 48,
          ),
          SizedBox(height: kDefaultPadding / 4),
          Text(
            'No Data Found!',
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(height: kDefaultPadding),
          OutlinedButton(
            onPressed: () {
              context.read<GuestBloc>().add(OnRefreshGuest());
            },
            child: Text(
              'Load Data',
              style: TextStyle(fontSize: 16),
            ),
            style: OutlinedButton.styleFrom(
              side: BorderSide(
                color: kPrimaryColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
