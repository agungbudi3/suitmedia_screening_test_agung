import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:suitmedia_screening_test_agung/controllers/guest/guest_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/models/guest.dart';
import 'package:suitmedia_screening_test_agung/screens/fourth/widgets/guest_empty.dart';
import 'package:suitmedia_screening_test_agung/screens/fourth/widgets/guest_item.dart';
import 'package:suitmedia_screening_test_agung/screens/fourth/widgets/guest_loading.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GuestGrid extends StatelessWidget {
  GuestGrid({
    Key? key,
    required this.onItemClick,
  }) : super(key: key);
  final Function(Guest) onItemClick;
  RefreshController _refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GuestBloc, GuestState>(
      listener: (c, s) {
        if (!s.isRefresh && _refreshController.isRefresh) {
          _refreshController.refreshCompleted();
        }
        if (!s.isLoading && _refreshController.isLoading) {
          _refreshController.loadComplete();
        }
        if (s.isError && s.errorMessage.isNotEmpty) {
          if (_refreshController.isLoading) {
            _refreshController.loadFailed();
          }
          if (_refreshController.isRefresh) {
            _refreshController.refreshFailed();
          }
        }
      },
      builder: (c, state) {
        var guests = state.guests;
        var isRefresh = state.isRefresh;
        var hasNext = state.hasNext;
        var selectedGuest = c.watch<MainBloc>().state.guest;
        if (isRefresh && guests.length == 0) {
          return GuestLoading();
        }

        if (!isRefresh && guests.length == 0) {
          return GuestEmpty();
        }

        return SmartRefresher(
          controller: _refreshController,
          onRefresh: () {
            context.read<GuestBloc>().add(OnRefreshGuest());
          },
          onLoading: () {
            context.read<GuestBloc>().add(OnLoadGuest());
          },
          enablePullDown: true,
          enablePullUp: hasNext,
          child: GridView.builder(
            itemCount: guests.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding / 2,
              vertical: kDefaultPadding / 2,
            ),
            itemBuilder: (ctx, idx) {
              var guest = guests[idx];
              return GuestItem(
                guest: guest,
                isSelected: selectedGuest?.id == guest.id,
                press: () {
                  onItemClick(guest);
                },
              );
            },
          ),
        );
      },
    );
  }
}
