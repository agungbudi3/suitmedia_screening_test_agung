
import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/models/guest.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class GuestItem extends StatelessWidget {
  const GuestItem({
    Key? key,
    required this.guest,
    this.isSelected = false,
    required this.press,
  }) : super(key: key);

  final Guest guest;
  final bool isSelected;
  final Function() press;

  @override
  Widget build(BuildContext context) {
    DecorationImage? image;
    Border? border;
    if (guest.avatar.isNotEmpty) {
      image = DecorationImage(
        image: NetworkImage(
          guest.avatar,
        ),
        fit: BoxFit.cover,
      );
    }
    if (isSelected) {
      border = Border.all(
        color: kPrimaryColor,
      );
    }

    return InkWell(
      onTap: !isSelected ? press : null,
      child: Container(
        margin: EdgeInsets.all(kDefaultPadding / 2),
        decoration: BoxDecoration(
          border: border,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: image,
                color: kPrimaryColor.withOpacity(0.5),
              ),
            ),
            SizedBox(height: kDefaultPadding),
            Text(
              "${guest.firstName} ${guest.lastName}",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
