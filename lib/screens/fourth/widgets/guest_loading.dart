import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class GuestLoading extends StatelessWidget {
  const GuestLoading({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(kDefaultPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 36,
            height: 36,
            child: CircularProgressIndicator(),
          ),
          SizedBox(height: kDefaultPadding),
          Text(
            'Loading data...',
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}