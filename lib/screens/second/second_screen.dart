import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/screens/second/widgets/input_choose_event.dart';
import 'package:suitmedia_screening_test_agung/screens/second/widgets/input_choose_guest.dart';
import 'package:suitmedia_screening_test_agung/screens/second/widgets/label_name.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var headerHeight = 221 / 360 * screenWidth;
    var footerHeight = 74 / 536 * screenWidth;
    return Scaffold(
      body: Container(
        height: double.infinity,
        child: Stack(
          children: [
            LabelName(),
            Positioned(
              top: headerHeight - 27,
              left: kDefaultPadding * 1.5,
              right: kDefaultPadding * 1.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InputChooseEvent(),
                  SizedBox(height: kDefaultPadding / 1.5),
                  InputChooseGuest(),
                ],
              ),
            ),
            Positioned(
              top: headerHeight + 27 + (kDefaultPadding * 1.5 / 2) + 54,
              bottom: footerHeight,
              left: kDefaultPadding * 3,
              right: kDefaultPadding * 3,
              child: Container(
                child: Image.asset("assets/images/img_suitmedia.png"),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AspectRatio(
                aspectRatio: 536 / 74,
                child: Image.asset("assets/images/img_bg_bottom.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
