import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_elevated_button.dart';
import 'package:suitmedia_screening_test_agung/utils/routes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InputChooseEvent extends StatelessWidget {
  const InputChooseEvent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var event = context.watch<MainBloc>().state.event;
    var text = event != null ? event.name : "Choose Event";
    return MyElevatedButton(
        height: 54,
        press: () {
          Navigator.pushNamed(context, thirdRoute);
        },
        text: text);
  }
}
