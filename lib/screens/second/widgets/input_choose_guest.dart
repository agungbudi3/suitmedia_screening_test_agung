import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/screens/widgets/my_elevated_button.dart';
import 'package:suitmedia_screening_test_agung/utils/routes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InputChooseGuest extends StatelessWidget {
  const InputChooseGuest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var guest = context.watch<MainBloc>().state.guest;
    var text =
        guest != null ? "${guest.firstName} ${guest.lastName}" : "Choose Guest";
    return MyElevatedButton(
        height: 54,
        press: () {
          Navigator.pushNamed(context, fourthRoute);
        },
        text: text);
  }
}
