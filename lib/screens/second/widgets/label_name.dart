import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class LabelName extends StatelessWidget {
  const LabelName({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var name = context.read<MainBloc>().state.name;
    return AspectRatio(
      aspectRatio: 360 / 221,
      child: Container(
        child: Stack(
          children: [
            Image.asset(
              "assets/images/bg_bright.png",
            ),
            SafeArea(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding * 2,
                ),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: kDefaultPadding * 1.5,
                      ),
                      child: Text.rich(
                        TextSpan(
                          text: "Hallo,\n",
                          style: TextStyle(
                            color: Color(0xff6e6e6e),
                            fontSize: 28,
                          ),
                          children: [
                            TextSpan(
                              text: name,
                              style: TextStyle(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: kDefaultPadding),
                            height: 1.5,
                            color: kPrimaryColor,
                          ),
                          SizedBox(height: kDefaultPadding / 2),
                          Container(
                            height: 1.5,
                            color: kPrimaryColor,
                          ),
                          SizedBox(height: kDefaultPadding / 2),
                          Container(
                            margin: EdgeInsets.only(left: kDefaultPadding),
                            height: 1.5,
                            color: kPrimaryColor,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
