import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/event/event_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/event_view/event_view_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/screens/third/widgets/event_list.dart';
import 'package:suitmedia_screening_test_agung/screens/third/widgets/event_map.dart';

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var eventView = context.watch<EventViewCubit>().state;
    IconData _getIcon() {
      switch (eventView) {
        case EventView.LIST:
          return Icons.map;
        case EventView.MAP:
          return Icons.format_list_bulleted;
      }
    }

    Widget _getBody() {
      switch (eventView) {
        case EventView.LIST:
          return EventList(
            onItemClick: (event) {
              context.read<MainBloc>().add(OnUpdateEvent(event));
              Navigator.pop(context);
            },
          );
        case EventView.MAP:
          return EventMap(
            onItemClick: (event) {
              context.read<MainBloc>().add(OnUpdateEvent(event));
              Navigator.pop(context);
            },
          );
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "EVENTS",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 19,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(_getIcon()),
            onPressed: () {
              context.read<EventViewCubit>().toggleView();
            },
          ),
        ],
        centerTitle: false,
      ),
      body: _getBody(),
    );
  }
}
