import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/models/event.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class EventItem extends StatelessWidget {
  const EventItem({
    Key? key,
    required this.event,
    required this.press,
    this.isSelected = false,
  }) : super(key: key);

  final Event event;
  final Function() press;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: !isSelected ? press : null,
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: kDefaultPadding / 2,
          vertical: kDefaultPadding / 2,
        ),
        decoration: BoxDecoration(
          border: Border.all(width: isSelected ? 3 : 0, color: kPrimaryColor),
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              color: Color(0x26000000),
              spreadRadius: 1,
              blurRadius: 4,
              offset: Offset(3, 4),
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                    color: kPrimaryColor.withOpacity(0.5),
                    image: DecorationImage(
                      image: NetworkImage(event.image),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(17),
                      bottomLeft: Radius.circular(17),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  padding: EdgeInsets.all(kDefaultPadding / 1.5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        event.name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: kDefaultPadding / 1.5,
                      ),
                      Expanded(
                        child: Text(
                          event.description,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(child: Container()),
                          Text(
                            event.date,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
