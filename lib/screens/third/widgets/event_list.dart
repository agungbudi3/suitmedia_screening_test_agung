import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test_agung/controllers/event/event_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/models/event.dart';
import 'package:suitmedia_screening_test_agung/screens/third/widgets/event_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class EventList extends StatelessWidget {
  const EventList({Key? key, required this.onItemClick}) : super(key: key);

  final Function(Event) onItemClick;

  @override
  Widget build(BuildContext context) {
    var events = context.watch<EventBloc>().state.events;
    var selectedEvent = context.watch<MainBloc>().state.event;
    return ListView.builder(
      padding: EdgeInsets.symmetric(
        vertical: kDefaultPadding / 2,
        horizontal: kDefaultPadding / 2,
      ),
      itemCount: events.length,
      itemBuilder: (ctx, idx) {
        return EventItem(
          isSelected: selectedEvent?.id == events[idx].id,
          event: events[idx],
          press: () {
            onItemClick(events[idx]);
          },
        );
      },
    );
  }
}
