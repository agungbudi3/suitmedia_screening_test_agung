import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:suitmedia_screening_test_agung/controllers/event/event_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/models/event.dart';
import 'package:suitmedia_screening_test_agung/screens/third/widgets/event_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';

class EventMap extends StatefulWidget {
  const EventMap({Key? key, required this.onItemClick}) : super(key: key);

  final Function(Event) onItemClick;

  @override
  _EventMapState createState() => _EventMapState();
}

class _EventMapState extends State<EventMap> {
  GoogleMapController? _googleMapController;
  PageController? _pageController;

  @override
  void initState() {
    super.initState();
    var selectedEvent = context.read<MainBloc>().state.event;
    if (selectedEvent != null) {
      context.read<EventBloc>().add(OnSelectEvent(selectedEvent.id));
    } else {
      context.read<EventBloc>().add(OnResetMarker());
    }
  }

  @override
  void dispose() {
    _googleMapController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var events = context.watch<EventBloc>().state.events;
    var markers = context.watch<EventBloc>().state.markers;
    var selectedEvent = context.watch<MainBloc>().state.event;
    var screenWidth = MediaQuery.of(context).size.width;
    var viewportFraction = 0.9;
    var cardWidth = (screenWidth - (kDefaultPadding / 2)) * viewportFraction;
    var cardHeight = cardWidth / 2;
    var initialPage = events.length * 2;
    if (selectedEvent != null) {
      var index = events.indexWhere((event) => selectedEvent.id == event.id);
      if (index >= 0) {
        initialPage = (events.length * 2) + index;
      }
    }
    _pageController = PageController(
      viewportFraction: viewportFraction,
      initialPage: initialPage,
    );

    return BlocListener<EventBloc, EventState>(
      listenWhen: (prev, curr) {
        return prev.selectedId != curr.selectedId && curr.selectedId >= 0;
      },
      listener: (c, s) {
        var idx = events.indexWhere((e) => e.id == s.selectedId);
        if (idx >= 0) {
          if (!c.read<EventBloc>().isScrolling) {
            c.read<EventBloc>().isAnimating = true;
            _pageController
                ?.animateToPage(
                  (events.length * 2) + idx,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.ease,
                )
                .then((value) => c.read<EventBloc>().isAnimating = false);
          } else {
            c.read<EventBloc>().isScrolling = false;
          }
        }
      },
      child: Stack(
        children: [
          GoogleMap(
            zoomControlsEnabled: false,
            myLocationButtonEnabled: false,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.94006033307907, 107.65580320509564),
              zoom: 13,
            ),
            markers: markers.toSet(),
            onMapCreated: (controller) {
              _googleMapController = controller;
            },
          ),
          Container(
            width: screenWidth,
            height: cardHeight,
            child: PageView.builder(
              //itemCount: events.length,
              onPageChanged: (i) {
                if (!context.read<EventBloc>().isAnimating) {
                  context.read<EventBloc>().isScrolling = true;
                  var idx = i % events.length;
                  if (events.length >= idx) {
                    context
                        .read<EventBloc>()
                        .add(OnSelectEvent(events[idx].id));
                  }
                }
              },
              controller: _pageController,
              itemBuilder: (c, i) {
                var idx = i % events.length;
                return Container(
                  width: cardWidth,
                  child: EventItem(
                    isSelected: selectedEvent?.id == events[idx].id,
                    event: events[idx],
                    press: () {
                      c.read<EventBloc>().add(OnSelectEvent(events[idx].id));
                      widget.onItemClick(events[idx]);
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
