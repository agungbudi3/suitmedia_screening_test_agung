import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFE7782D);

const kDefaultPadding = 20.0;
const kConnectionTimeout = 60000;
const kConnectionReadTimeout = 30000;
const kConnectionWriteTimeOut = 30000;