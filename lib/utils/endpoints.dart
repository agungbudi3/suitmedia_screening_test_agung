import 'package:dio/dio.dart';
import 'package:suitmedia_screening_test_agung/models/guest_res.dart';
import 'package:suitmedia_screening_test_agung/utils/constants.dart';
import 'package:suitmedia_screening_test_agung/utils/network/api_result.dart';
import 'package:suitmedia_screening_test_agung/utils/network/network_exceptions.dart';

class Endpoints {
  late Dio _dio;

  Endpoints() {
    _dio = Dio();

    _dio.options.baseUrl = "https://reqres.in/";

    _dio.options.connectTimeout = kConnectionTimeout;
    _dio.options.receiveTimeout = kConnectionReadTimeout;

    _dio.options.headers['content-type'] = 'application/json';
  }

  Future<ApiResult<GuestRes>> getGuest({
    int? page,
  }) async {
    try {
      final response = await _dio.get(
        'api/users',
        queryParameters: {
          'page': page ?? 1,
        },
      );
      return ApiResult.success(GuestRes.fromJson(response.data));
    } catch (e, s) {
      print(e);
      print(s);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
