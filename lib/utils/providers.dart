import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/event/event_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/event_view/event_view_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/guest/guest_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_palindrome/input_palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/main/main_bloc.dart';
import 'package:suitmedia_screening_test_agung/controllers/input_name/input_name_cubit.dart';
import 'package:suitmedia_screening_test_agung/controllers/palindrome/palindrome_cubit.dart';
import 'package:suitmedia_screening_test_agung/utils/endpoints.dart';

List<BlocProvider> bloc = [
  BlocProvider<MainBloc>(create: (c) => MainBloc()),
  BlocProvider<InputNameCubit>(create: (c) => InputNameCubit()),
  BlocProvider<InputPalindromeCubit>(create: (c) => InputPalindromeCubit()),
  BlocProvider<EventBloc>(create: (c) => EventBloc()..add(OnLoadEvent())),
  BlocProvider<GuestBloc>(
      create: (c) => GuestBloc(
            RepositoryProvider.of<Endpoints>(c),
          )),
  BlocProvider<PalindromeCubit>(create: (c) => PalindromeCubit()),
  BlocProvider<EventViewCubit>(create: (c) => EventViewCubit()),
];
