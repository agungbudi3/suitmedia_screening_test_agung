import 'package:suitmedia_screening_test_agung/screens/first/first_screen.dart';
import 'package:suitmedia_screening_test_agung/screens/fourth/fourth_screen.dart';
import 'package:suitmedia_screening_test_agung/screens/second/second_screen.dart';
import 'package:suitmedia_screening_test_agung/screens/third/third_screen.dart';

const firstRoute = '/';
const secondRoute = '/second';
const thirdRoute = '/third';
const fourthRoute = '/fourth';

var routes = {
  firstRoute: (context) => FirstScreen(),
  secondRoute: (context) => SecondScreen(),
  thirdRoute: (context) => ThirdScreen(),
  fourthRoute: (context) => FourthScreen(),
};
